#!/bin/bash
export PS1='\[$(tput bold)\]\[$(tput setaf 4)\]\W >\[$(tput sgr0)\] '
export TERM='xterm-256color'
export PATH="~/.local/bin/:$PATH"
shopt -s checkwinsize
