#+TITLE: VKB13's Personal GNU Emacs Configuration
#+AUTHOR: VKB13
#+STARTUP: indent, overview

* About this config
  This is my personal GNU Emacs config which i use for everyday work ranging from programming to sending emails.

* Initial dependencies and essential packages
  Firstly in the config we want to load and setup packages that we will need in the rest of the config.

** Setup package.el to work with the MELPA repository
   #+begin_src emacs-lisp
   (require 'package)
   (add-to-list 'package-archives
		'("melpa" . "https://melpa.org/packages/"))
   (unless 'package-refresh-contents
     (package-refresh-contents))
   (package-initialize)
   #+end_src

** Install and configure the use-package macro
   Use-package removes the hassle of installing packages over and over on different systems by providing a macro for installing packages, lazy loading and dependencies.

   #+begin_src emacs-lisp
   (unless (package-installed-p 'use-package)
     (package-install 'use-package))
   (setq use-package-always-ensure t)
   #+end_src

** Evil Mode
   Evil is a extensible layer for emacs which emulates most of the Vi keybindings in emacs allowing for the extensibility of emacs while not damaging your wrists. Here we also use the use-package macro for the first time.

   #+begin_src emacs-lisp
   (use-package evil
     :init
     (setq evil-vsplit-window-right t)
     (setq evil-split-window-below t)
     (evil-mode))
   #+end_src

* Startup performance
  Emacs can take a long time to startup. we can improve this by tweaking it's garbage collection settings and installing the GCMH package.

  #+begin_src emacs-lisp
  (use-package gcmh
    :config
    (gcmh-mode 1))

  (setq gc-cons-threshold 402653184
	gc-cons-percentage 0.6)

  (setq comp-async-report-warnings-errors nil)
  (setq gc-cons-threshold (* 2 1000 1000))
  #+end_src

* Aesthetics
  Emacs looks pretty ugly by default so here we enable line numbers and truncated lines, disable the scrollbar, menubar and toolbar, set the fonts, install all the icons for icon support, and install our theme, dashboard and modeline.

** Line numbers and truncated lines
   #+begin_src emacs-lisp
   (global-display-line-numbers-mode 1)
   (global-visual-line-mode t)
   #+end_src
  
** Disable the menubar, scrollbar and toolbar
   #+begin_src emacs-lisp
   (menu-bar-mode -1)
   (tool-bar-mode -1)
   (scroll-bar-mode -1)
   #+end_src
  
** Fonts
   #+begin_src emacs-lisp
   (add-to-list 'default-frame-alist '(font . "monospace-14"))
   (set-face-attribute 'default nil
		       :font "monospace"
		       :weight 'medium)
   (set-face-attribute 'variable-pitch nil
		       :font "sans-serif"
		       :weight 'medium)
   (set-face-attribute 'fixed-pitch nil
		       :font "sans-serif"
		       :weight 'medium)

   (set-face-attribute 'font-lock-comment-face nil
		       :slant 'italic)
   (set-face-attribute 'font-lock-keyword-face nil
		       :slant 'italic)
   #+end_src
  
** All the icons

   (use-package all-the-icons)
  
** Dashboard
   
   #+begin_src emacs-lisp
   (use-package dashboard
     :init
     (setq dashboard-set-heading-icons t)
     (setq dashboard-set-file-icons t)
     (setq dashboard-banner-logo-title "Welcome to GNU Emacs!")
     (setq dashboard-startup-banner 'logo)
     (setq dashboard-center-content t)
     :config
     (dashboard-setup-startup-hook))
   #+end_src

** Dashboard in Emacsclient

   #+begin_src emacs-lisp
   (setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))
   #+end_src

** Doom themes and Doom modeline

   #+begin_src emacs-lisp
   (use-package doom-themes
     :config
     (load-theme 'doom-one t))

   (use-package doom-modeline
     :init
     (doom-modeline-mode 1))
   #+end_src

* Ivy Mode
  Ivy, counsel and swiper are generic completion mechanics for emacs. Ivy-rich allows us to add descriptions alongside the commands in M-x.

** Installing Ivy and basic settings
   #+begin_src emacs-lisp
   (use-package counsel
     :after ivy
     :config (counsel-mode))
   (use-package ivy
     :defer 0.1
     :diminish
     :bind
     (("C-c C-r" . ivy-resume)
      ("C-x B" . ivy-switch-buffer-other-window))
     :custom
     (setq ivy-count-format "(%d/%d) ")
     (setq ivy-use-virtual-buffers t)
     (setq enable-recursive-minibuffers t)
     :config
     (ivy-mode))
   (use-package ivy-rich
     :after ivy
     :custom
     (ivy-virtual-abbreviate 'full
			     ivy-rich-switch-buffer-align-virtual-buffer t
			     ivy-rich-path-style 'abbrev)
     :config
     (ivy-set-display-transformer 'ivy-switch-buffer
				  'ivy-rich-switch-buffer-transformer)
     (ivy-rich-mode 1))
   (use-package swiper
     :after ivy
     :bind (("C-s" . swiper)
	    ("C-r" . swiper)))
   #+end_src

** Fixing M-x
   #+begin_src emacs-lisp
   (setq ivy-initial-inputs-alist nil)
   (use-package smex
     :config
     (smex-initialize))
   #+end_src

* Language support
  Adds support for languages that are not supported by default in emacs.

  #+begin_src emacs-lisp
  (use-package haskell-mode)
  (use-package lua-mode)
  (use-package markdown-mode)
  #+end_src

* ORG Mode
  Org mode is fantastic, but i like to add some improvements such as org bullets instead of asteriks and better source code syntax highlighting.

  #+begin_src emacs-lisp
  (use-package org-bullets)
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
  (setq org-src-fontify-natively t
	org-src-tab-acts-natively t
	org-confirm-babel-evaluate nil
	org-edit-src-content-indentation 0)
  #+end_src
  
* Misc features
  Here i put some keybindings and features that are not needed but nice to have such as scrolling out and in with mouse, emojis and text replacing by mouse selection when typing.

  #+begin_src emacs-lisp
  (global-set-key (kbd "C-=") 'text-scale-increase)
  (global-set-key (kbd "C--") 'text-scale-decrease)
  (global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
  (global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)

  (delete-selection-mode t)

  (use-package emojify
    :hook (after-init . global-emojify-mode))
  #+end_src
