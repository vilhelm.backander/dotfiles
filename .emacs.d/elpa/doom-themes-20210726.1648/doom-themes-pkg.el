(define-package "doom-themes" "20210726.1648" "an opinionated pack of modern color-themes"
  '((emacs "25.1")
    (cl-lib "0.5"))
  :commit "4c83bd7d1099c352338bce6fea2473c035b39b1b" :authors
  '(("Henrik Lissner <https://github.com/hlissner>"))
  :maintainer
  '("Henrik Lissner" . "henrik@lissner.net")
  :keywords
  '("custom themes" "faces")
  :url "https://github.com/hlissner/emacs-doom-themes")
;; Local Variables:
;; no-byte-compile: t
;; End:
