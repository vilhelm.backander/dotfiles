import XMonad
import XMonad.Layout.Spacing
import XMonad.Util.SpawnOnce
import XMonad.Hooks.DynamicLog
import Data.Monoid
import System.Exit
import qualified XMonad.StackSet as W
import qualified Data.Map        as M

myTerminal      = "alacritty"
myAppLauncher   = "dmenu_run"
myTextEditor    = "emacsclient -c -a emacs"
myStatusBar     = "xmobar"
myStatusBarTheme = xmobarPP { ppCurrent               = xmobarColor "#98be65" "" . wrap "[""]"
                            , ppHiddenNoWindows       = xmobarColor "#bbc2cf" ""
                            , ppLayout                = xmobarColor "#51afef" ""
                            , ppSep                   = xmobarColor "#5B6268" "" $ " | "
                            , ppTitle                 = xmobarColor "#ff6c6b" "" . shorten 35 }
myWindowSpacing = 8
myBorderWidth   = 2
myModMask       = mod1Mask
myWorkspaces    = ["1","2","3","4","5","6","7","8","9"]
myNormalBorderColor  = "#3f444a"
myFocusedBorderColor = "#c678dd"

myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
    [ ((modm .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf)
    , ((modm,               xK_p     ), spawn myAppLauncher)
    , ((modm .|. shiftMask, xK_s     ), spawn myTextEditor)
    , ((modm,               xK_q     ), kill)
    , ((modm,               xK_space ), sendMessage NextLayout)
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)
    , ((modm,               xK_n     ), refresh)
    , ((modm,               xK_Tab   ), windows W.focusDown)
    , ((modm,               xK_j     ), windows W.focusDown)
    , ((modm,               xK_k     ), windows W.focusUp  )
    , ((modm,               xK_m     ), windows W.focusMaster  )
    , ((modm,               xK_Return), windows W.swapMaster)
    , ((modm .|. shiftMask, xK_j     ), windows W.swapDown  )
    , ((modm .|. shiftMask, xK_k     ), windows W.swapUp    )
    , ((modm,               xK_h     ), sendMessage Shrink)
    , ((modm,               xK_l     ), sendMessage Expand)
    , ((modm,               xK_t     ), withFocused $ windows . W.sink)
    , ((modm,               xK_comma ), sendMessage (IncMasterN 1))
    , ((modm,               xK_period), sendMessage (IncMasterN (-1)))
    , ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))
    ]
    ++
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++
    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

myLayoutHook = tiled ||| Full
  where
     tiled   = spacing myWindowSpacing $ Tall nmaster delta ratio
     nmaster = 1
     ratio   = 1/2
     delta   = 3/100

myStartupHook = do
  spawnOnce "setxkbmap se"
  spawnOnce "nitrogen --restore"
  spawnOnce "picom &"
  spawnOnce "emacs --daemon &"

main = xmonad =<< statusBar myStatusBar myStatusBarTheme def defaults

defaults = def {
        terminal           = myTerminal,
        focusFollowsMouse  = True,
        clickJustFocuses   = False,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,
        keys               = myKeys,
        layoutHook         = myLayoutHook,
        startupHook        = myStartupHook,
        handleEventHook    = def,
        manageHook         = def,
        mouseBindings      = def,
        logHook            = def
}
